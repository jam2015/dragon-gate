﻿// (Unity3D) New monobehaviour script that includes regions for common seactions, debug, versioning and schedule.
using UnityEngine;
using System.Collections;

public class PointsObj : MonoBehaviour
{
    #region GlobalVareables
    #region DefaultVareables
    public bool isDebug = false;
    private string debugScriptName = "PointsObj";
    #endregion

    #region Public
    public int points = 1;
    #endregion

    #region Private

    #endregion
    #endregion

    #region CustomFunction
    #region Public

    #endregion

    #region Private

    #endregion

    #region Debug
    private void PrintDebugMsg(string msg)
    {
        if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters
    public int GetPoints()
    {
        return points;
    }
    #endregion

    #region Setters

    #endregion
    #endregion

    #region UnityFunctions

    #endregion

    #region Start_Update
    // Use this for initialization
    void Start()
    {
        if (isDebug) Debug.Log(debugScriptName + ": Loaded.");
    }
    // Called every fixed frame
    void FixedUpdate()
    {

    }
    // Called each frame
    void Update()
    {

    }
    #endregion
}

/*
 * Changelog
 * 
 * (1/30/2016 1:24:20 AM)
 * 0- Created
*/
