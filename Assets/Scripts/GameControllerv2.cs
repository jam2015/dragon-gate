﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameControllerv2 : MonoBehaviour {

	private List<GameObject> player1Trail;
	private List<GameObject> player2Trail;
	private List<GameObject> currentPlayerTrail;
	private GameObject currentPlayerTrailRef;
	private GameObject currentPlayer;
	private bool gameStart = false;
	private bool waitForPlayer = true;
	private double playerPathTime = 0;
	private double PlayerAcumulatingTime = 0;

	public GameObject player1;
	public GameObject player2;
	public GameObject player1TrailRef;
	public GameObject player2TrailRef;
	public GameState cState; // controlls game state
	public double timeIncrement = 50;
	public double currentPlayerTime;
	public GameObject playerSelectUI;
	public GameObject gameoverMenu;
	public double turnTimmer;
	public double player1Score;
	public double player2Score;
	public GameObject winningPlayer;
	public double pointsAdd;
	public double pointsSub;
	public double pointsRock;
	public GameObject scoreGUI;
	public Text cPlayScore;
	public GameObject winningPlayer1;
	public GameObject winningPlayer2;




	// Use this for initialization
	void Start () {
		player1Trail = new List<GameObject> ();
		player2Trail = new List<GameObject> ();
		playerSelectUI.gameObject.SetActive (false);
		scoreGUI.gameObject.SetActive (false);
		player1.GetComponent<Player> ().playerID = 1;
		player2.GetComponent<Player> ().playerID = 2;

		winningPlayer1.SetActive (false);
		winningPlayer2.SetActive (false);

		player1Score = 50;
		player2Score = 50;

		pointsAdd = 3;
		pointsSub = 2;
		pointsRock = 1000000;
	}

	// Update is called once per frame
	void FixedUpdate () {
		switch (cState) {
		case GameState.Startup:
			if (scoreGUI.gameObject.activeSelf ==false) {
				scoreGUI.gameObject.SetActive (true);
			}
			if (playerSelectUI.gameObject.activeSelf == false) {
				playerSelectUI.gameObject.SetActive (true);
			}

			if (gameStart == true) {
				
				currentPlayer=player1;
				currentPlayerTrail = player1Trail;
				currentPlayerTrailRef = player1TrailRef;
				cState = GameState.WaitForPlayer;
				waitForPlayer = true;
				playerSelectUI.gameObject.SetActive (false);

			}
			break;
		case GameState.WaitForPlayer:
			scoreGUI.gameObject.SetActive (true);
			if (Input.GetButtonDown("Jump"))
				waitForPlayer = false;
			if (waitForPlayer == false) {
				// clear current player path
				currentPlayer.GetComponent<Player> ().ToggleIsActve ();
				cState = GameState.FollowPath;
				PlayerAcumulatingTime += timeIncrement;
				turnTimmer = PlayerAcumulatingTime;
			}
			break;
		case GameState.FollowPath:
			scoreGUI.gameObject.SetActive (true);

			if (turnTimmer <= timeIncrement) {
				cState = GameState.AddPath;
			}
			turnTimmer -= Time.deltaTime;
			if (currentPlayer.gameObject.GetComponent<Player>().playerID == 1) {
				player1Score -= pointsSub;
				if (player1Score < 0) {
					winningPlayer1.transform.position = player1.transform.position;
					CurrentPlayerGameOver ();
				}
				cPlayScore.text = player1Score.ToString();
			} else if (currentPlayer.gameObject.GetComponent<Player>().playerID == 2) {
				player2Score -= pointsSub;
				if (player2Score < 0) {
					winningPlayer2.transform.position = player1.transform.position;
					CurrentPlayerGameOver ();
				}
				cPlayScore.text = player2Score.ToString();
			}



			break;
		case GameState.AddPath:
			scoreGUI.gameObject.SetActive (true);
			if (turnTimmer <= 0) {
				nextPlayer ();
				cState = GameState.WaitForPlayer;
				waitForPlayer = true;
			}
			turnTimmer -= Time.deltaTime;
			break;
		case GameState.GameOver:
			currentPlayer.GetComponent<Player> ().ToggleIsActve ();
			gameoverMenu.SetActive (true);
			scoreGUI.SetActive (true);

			if (winningPlayer.gameObject.GetComponent<Player>().playerID == 1) {
				winningPlayer1.SetActive (true);
				cPlayScore.text = player1Score.ToString ();
			}else if (winningPlayer.gameObject.GetComponent<Player>().playerID == 2){
			winningPlayer2.SetActive (true);
				cPlayScore.text = player2Score.ToString ();
			}


			break;
		}


	}

	public bool CanScore(){
		if (cState == GameState.FollowPath) {
			return true;
		} else {
			return false;
		}
	}

	public bool makePath(){
		bool val = false;
		if (cState == GameState.AddPath)
			val = true;
		else
			val = false;
		GameObject newPath = Instantiate (currentPlayerTrailRef);
		newPath.transform.position = currentPlayer.transform.position;
		currentPlayerTrail.Add (newPath);
		return val;

	}

	public void clearCurrentPlayerTrail(){
		foreach (GameObject trail in currentPlayerTrail) {
			Destroy (trail);
		}
		currentPlayerTrail.Clear ();
	}

	private void nextPlayer(){
		currentPlayer.GetComponent<Player> ().ToggleIsActve ();

		if (currentPlayer.gameObject.GetComponent<Player>().playerID == 1) {
			currentPlayerTrail = player2Trail;
			currentPlayer = player2;
			currentPlayerTrailRef = player2TrailRef;
		} else {
			currentPlayer=player1;
			currentPlayerTrail = player1Trail;
			currentPlayerTrailRef = player1TrailRef;
		}
		clearCurrentPlayerTrail ();
	}

	public void StartGame(){
		gameStart = true;
	}

	public void removeActivePlayer(){
		
		cState = GameState.GameOver;
	}

	public void CurrentPlayerGameOver(){
		if (currentPlayer.gameObject.GetComponent<Player>().playerID == 1) {
			WinningPlayer(player2);
		} else {
			WinningPlayer(player1);
		}
		cState = GameState.GameOver;
	}

	public void PlayerAddPoint(GameObject player){
		Debug.Log ("asking for score");
		if (player.gameObject.GetComponent<Player>().playerID == 1 && currentPlayer.gameObject.GetComponent<Player>().playerID == 1) {
				player1Score += pointsAdd;
		}else if(player == player2&& currentPlayer == player2){
				player2Score += pointsAdd;
		}
	}

	public void PlayerReducePoint(GameObject player){
		Debug.Log ("Hit A ROCK");
		if (player.gameObject.GetComponent<Player>().playerID == 1 && currentPlayer.gameObject.GetComponent<Player>().playerID == 1) {
			player1Score -= pointsRock;
		} else if (player.gameObject.GetComponent<Player>().playerID == 2 && currentPlayer.gameObject.GetComponent<Player>().playerID == 2) {
			player2Score -= pointsRock;
		} else {
			Debug.Log ("Hit but cant find player");
		}
	}

	public void WinningPlayer(GameObject player){
		winningPlayer = player;
		cState = GameState.GameOver;
	}

	public void RestartLevel(){
		Application.Quit ();
	}

	public enum GameState {Startup, WaitForPlayer, FollowPath, AddPath, GameOver};

	}
