﻿// (Unity3D) New monobehaviour script that includes regions for common seactions, debug, versioning and schedule.
using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
    #region GlobalVareables
    #region DefaultVareables
    public bool isDebug = false;
    private string debugScriptName = "Player";
    #endregion

    #region Public
    public bool isActive = false;
    public GameObject background = null;
    public float vertSpd = 5;
    public float horizSpd = 5;
    public float victorySpd = 7.5f;
    public float leftLimit = -5;
    public float rightLimit = 5;
    //public GameObject backgroundPrefab = null;
    public float applyScoreCoolDown = 1;
    public float sendMakePathCoolDown = .01f;
    public int offPathPenalty = 1;
    public float animActiveSpd = 1;
    public float animInactiveSpd = .5f;
    public float horizForceMax = 100;
    public float vertForceMax = 100;
	public int playerID;
    public float scoreCooldown = 1;
    #endregion

    #region Private
    private string playerName = "NO_NAME_ASSIGNED";
	private GameControllerv2 gameControllerScriptv2 = null;
    private Animator fishAnim = null;
    private Vector2 startPos = Vector2.zero;
    private float currVertSpd = 0;
    private float currHorizSpd = 0;
    private int score = 0;
    private float lastScore = 0;
    private float lastPath = 0;
    private bool onPath = false;
    private bool noPointsGainedYet = true;
    private bool isFalling = false;
    private bool doVictoryPB = false;
    private float victoryStart = 0;
    #endregion
    #endregion

    #region CustomFunction
    #region Public
    // Increase the speed of the player by the amount passed
    public void IncreaseVertSpd(float amount)
    {
        currVertSpd += amount;
    }
    public void IncreaseHorizSpd(float amount)
    {
        currHorizSpd += amount;
    }

    // Return the player to its normal starting speed
    public void ReturnNormalVertSpd()
    {
        currVertSpd = vertSpd;
    }
    public void ReturnNormalHorizSpd()
    {
        currHorizSpd = horizSpd;
    }

    // Toggle the isActive variable and move the player eithr aside or back in play
    public void ToggleIsActve()
    {
        if (isActive)
        {
            isActive = false;
            Fall();
        }
        else
        {
            isActive = true;
            background.transform.GetChild(0).GetComponent<Camera>().enabled = true;
            this.transform.position = startPos;
            fishAnim.speed = animActiveSpd;
        }
    }

    // Add/subtract to the score of the player
    public void ApplyScore(int amount)
    {
        score += amount;

        if (score < 0) score = 0;
        else if (score == 0 && !noPointsGainedYet)
        {
            PrintDebugMsg("Player is out.");
            gameControllerScriptv2.removeActivePlayer();
        }
        else
        {
            if (noPointsGainedYet)
            {
                PrintDebugMsg("Player now vulnerable to loosing.");
                noPointsGainedYet = false;
            }
            PrintDebugMsg("Applying " + amount + " to score. Score is now " + score + ".");
        }
    }

    // The end game animation where the winner swims past the looser at thier point where they lost and falling
    public void VictoryPassBy(Vector2 otherPlayerPos)
    {
        this.transform.position = otherPlayerPos;
        this.transform.Translate(Vector2.down * 10);
        victoryStart = this.transform.position.y;
        doVictoryPB = true;
    }
    #endregion

    #region Private
    // Move the player up constently with the background
    // Apply the player's left and right input
    // Keep player within the left and right edges
    private void Move()
    {
        this.transform.Translate(Vector2.up * (currVertSpd * .1f));
        background.transform.position = new Vector2(0, (this.transform.position.y));

        this.transform.Translate(new Vector2((Input.GetAxis("Horizontal") * currHorizSpd * .1f), 0));

        if (this.transform.position.x < leftLimit) this.transform.position = new Vector2(leftLimit, this.transform.position.y);
        else if (this.transform.position.x > rightLimit) this.transform.position = new Vector2(rightLimit, this.transform.position.y);
    }

    // Check if off the path and deduct points
    private void CheckOffPath()
    {
        if (!onPath && Time.time - lastScore >= applyScoreCoolDown)
        {
            PrintDebugMsg("Not on path.");
            bool addingNewPath = gameControllerScriptv2.makePath();
            if(!addingNewPath) ApplyScore(-offPathPenalty);

            lastScore = Time.time;
        }
    }

    // Fall from the top to bottom using the rigidbody
    private void Fall()
    {
        Rigidbody2D body = this.gameObject.GetComponent<Rigidbody2D>();
        body.isKinematic = false;
        body.AddForce(new Vector2(Random.Range(-horizForceMax, horizForceMax), Random.Range(vertForceMax / 2, vertForceMax)));
        fishAnim.speed = animInactiveSpd;
        isFalling = true;
    }

    // The second part of making us inactive after done falling
    private void FinsishToggleIsActive()
    {
        //background.transform.GetChild(0).GetComponent<Camera>().enabled = false;
        this.transform.position = startPos;
        //background.transform.position = new Vector2(0, (this.transform.position.y));
        this.transform.Translate(Vector2.right * 10);
        this.gameObject.GetComponent<Rigidbody2D>().isKinematic = true;
    }
    #endregion

    #region Debug
    private void PrintDebugMsg(string msg)
    {
        if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters
    public bool GetIsActive()
    {
        return isActive;
    }
    public string GetPlayerName()
    {
        return playerName;
    }
    public int GetScore()
    {
        return score;
    }
    #endregion

    #region Setters
    public void SetPlayerName(string name)
    {
        playerName = name;
    }
    #endregion
    #endregion

    #region UnityFunctions
    public void OnTriggerStay2D(Collider2D otherObj)
    {
		//Debug.Log("Triggering an object.");

        
		if(gameControllerScriptv2.CanScore())
        {
			int colPID = otherObj.GetComponent<TrailValues> ().playerID;
			if (playerID != colPID) gameControllerScriptv2.PlayerAddPoint (this.gameObject);
		}
    }

    public void OnTriggerEnter2D(Collider2D otherObj)
    {
        PrintDebugMsg("Triggered an object.");

		PointsObj pointsObjScript = otherObj.GetComponent<PointsObj>();
		if(pointsObjScript != null)
		{
			gameControllerScriptv2.PlayerReducePoint(this.gameObject);
			lastScore = Time.time;

		}
        
        if (otherObj.GetComponent<PointsObj>() != null)
        {
            PrintDebugMsg("Triggered an object with \"PointsObj.cs\".");
            onPath = true;
        }
    }

    public void OnTriggerExit2D(Collider2D otherObj)
    {
        PrintDebugMsg("No longer triggering an object.");
        onPath = false;
    }
	/*
    public void OnGUI()
    {
        GUI.Label(new Rect(0, 0, 1000, 20), "Score: " + score);
    }
    */
    #endregion

    #region Start_Update
    // Use this for initialization
    void Start()
    {
        if (isDebug) Debug.Log(debugScriptName + ": Loaded.");

        startPos = this.transform.position;
        currVertSpd = vertSpd;
        currHorizSpd = horizSpd;
        lastScore = Time.time;
        gameControllerScriptv2 = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameControllerv2>();
        fishAnim = this.gameObject.transform.GetChild(0).gameObject.GetComponent<Animator>();
        //background = (GameObject)Instantiate(backgroundPrefab, this.transform.position, Quaternion.identity);
        //background.transform.GetChild(0).GetComponent<Camera>().enabled = false;
    }
    // Called every fixed frame
    void FixedUpdate()
    {
        if (isFalling)
        {
            PrintDebugMsg("Still falling...");
            if (this.transform.position.y < Vector2.zero.y - 5)
            {
                PrintDebugMsg("Done falling.");
                isFalling = false;
                FinsishToggleIsActive();
            }
        }
        else if(doVictoryPB)
        {
            PrintDebugMsg("Doing a pass by...");
            if(this.transform.position.y < (victoryStart + 10))
            {
                this.transform.Translate(Vector2.up * (currVertSpd * victorySpd));
                fishAnim.speed = animInactiveSpd;
            }
        }
        else if (isActive)
        {
            Move();
            CheckOffPath();
        }
    }
    // Called each frame
    void Update()
    {
        if (isDebug && Input.GetKeyDown("h"))
        {
            PrintDebugMsg("Toggling isActive...");
            ToggleIsActve();
        }
    }
    #endregion
}

/*
 * Changelog
 * 
 * (1/29/2016 11:56:50 PM)
 * 0- Created
*/
