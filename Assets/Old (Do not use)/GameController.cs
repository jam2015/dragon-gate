﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {
	private List<GameObject> player1PathList; // array of paths
	private List<GameObject> player2PathList; // array of paths
	private List<GameObject> currentPlayerPath;
	private bool startTimmer = false;
	private float pathTimer = 0;
	private bool waitForPlayer = true;
	private GameObject currentPlayerPathRef;

	public GameObject activePlayer; //keeps track of active player
	public GameState cState; // controlls game state
	public GameObject activePath;
	public int currentPath;
	public int pathNum;
	public float updateFrequency = 1;
	public float pathTime = 200;
	public GameObject playerSelectUI;
	public bool gameStart;
	public GameObject player1;
	public GameObject player2;
	public GameObject player1Path;
	public GameObject player2Path;
	public int pathPerTurn = 100;


	void Start () {
		player1PathList = new List<GameObject> ();
		player2PathList = new List<GameObject> ();
		gameStart = false;

		cState = GameState.Startup;
	}



	void FixedUpdate () {
		switch (cState) {
		case GameState.Startup:
			if (gameStart == true) {
					

				activePlayer=player1;
				currentPlayerPath = player1PathList;
				currentPlayerPathRef = player1Path;
				cState = GameState.WaitForPlayer;
				waitForPlayer = true;
				playerSelectUI.gameObject.SetActive (false);

			}
			break;
		case GameState.WaitForPlayer:
			if (Input.GetButtonDown("Jump"))
				waitForPlayer = false;
			if (waitForPlayer == false) {
				activePlayer.GetComponent<Player> ().ToggleIsActve ();
				cState = GameState.FollowPath;
				currentPath = 0;
				pathNum = currentPlayerPath.Count;
			}
			break;
		case GameState.FollowPath:
			if (startTimmer == true) {
				pathTimer = pathTime;
				cState = GameState.AddPath;
			}
			break;
		case GameState.AddPath:
			if (pathTimer < 0) {
				startTimmer = false;
				nextPlayer ();
				cState = GameState.WaitForPlayer;
				waitForPlayer = true;
			}
			pathTimer--;
			break;
		case GameState.GameOver:
			;
			break;
		}


	}



	private void nextPlayer(){
		activePlayer.GetComponent<Player> ().ToggleIsActve ();

		if (activePlayer == player1) {
			currentPlayerPath = player2PathList;
			activePlayer = player2;
			currentPlayerPathRef = player2Path;
		} else {
			activePlayer=player1;
			currentPlayerPath = player1PathList;
			currentPlayerPathRef = player1Path;
		}
	}

	public bool makePath(){
		if (currentPath >= pathNum) {
			GameObject newPath = Instantiate (currentPlayerPathRef);
			newPath.transform.position = activePlayer.transform.position;
			currentPlayerPath.Add (newPath);
			pathNum++;
			currentPath++;
			startTimmer = true;
			return true;
		} else if (cState == GameState.FollowPath || cState == GameState.AddPath) {
			currentPlayerPath [currentPath].transform.position = activePlayer.transform.position;
			currentPath++;
			return false;
		} else {
			return false;
		}
	}

	public void setNumPlayers (int num){
		gameStart = true;
	}

	public void removeActivePlayer(){

	}

	public void clearPath(){
		
		}

	public enum GameState {Startup, WaitForPlayer, FollowPath, AddPath, GameOver};
}
