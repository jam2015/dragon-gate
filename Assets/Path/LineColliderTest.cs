﻿// (Unity3D) New monobehaviour script that includes regions for common seactions, debug, versioning and schedule.
using UnityEngine;
using System.Collections;

public class LineColliderTest : MonoBehaviour
{
    #region GlobalVareables
    #region DefaultVareables
    public bool isDebug = false;
    private string debugScriptName = "LineColliderTest";
    #endregion

    #region Public
    public float width = 1;
    public Vector2 s = Vector2.zero;
    public Vector2 e = Vector2.zero;
    #endregion

    #region Private
    private BoxCollider2D coll = null;
    private Vector2 startPos = Vector2.zero;
    private Vector2 endPos = Vector2.zero;
    #endregion
    #endregion

    #region CustomFunction
    #region Public
    // Assign the starting point and end point
    public void AssignPoints(Vector2 start, Vector2 end)
    {
        startPos = start;
        endPos = end;

        UpdateColl();
    }
    #endregion

    #region Private
    // Update the position, rotation, and scale of collider using given points
    private void UpdateColl()
    {
        if(this.GetComponent<BoxCollider2D>() == null) coll = this.gameObject.AddComponent<BoxCollider2D>();
        else coll = this.GetComponent<BoxCollider2D>();

        coll.offset = Vector2.zero;
        coll.transform.localScale = new Vector2(width, Vector2.Distance(startPos, endPos));
        coll.transform.position = new Vector2(((endPos.x - startPos.x) / 2), ((endPos.y - startPos.y) / 2));
        
        float angle = Mathf.Atan2(endPos.y, endPos.x) * Mathf.Rad2Deg;
        PrintDebugMsg("End position on the right. | Angle to rotate: " + angle);
        coll.transform.localEulerAngles = new Vector3(0, 0, -angle);
    }
    #endregion

    #region Debug
    private void PrintDebugMsg(string msg)
    {
        if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters

    #endregion

    #region Setters

    #endregion
    #endregion

    #region UnityFunctions

    #endregion

    #region Start_Update
    // Use this for initialization
    void Start()
    {
        if (isDebug) Debug.Log(debugScriptName + ": Loaded.");

        AssignPoints(s, e);
    }
    // Called every fixed frame
    void FixedUpdate()
    {

    }
    // Called each frame
    void Update()
    {

    }
    #endregion
}

/*
 * Changelog
 * 
 * (1/30/2016 10:59:52 AM)
 * 0- Created
*/
